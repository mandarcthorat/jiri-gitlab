from click.testing import CliRunner
from functools import partial
from xml.etree import ElementTree as ET
import pytest

from jiri_gitlab.cli import create_manifest, list_projects


@pytest.fixture()
def run_create_manifest():
    runner = CliRunner()
    return partial(runner.invoke, create_manifest)


@pytest.fixture()
def run_list_projects():
    runner = CliRunner()
    return partial(runner.invoke, list_projects)


def test_list_projects(tmp_path, run_list_projects):
    manifest = tmp_path / 'test.xml'
    # from https://fuchsia.googlesource.com/jiri#managing-your-projects-with-jiri
    manifest.write_text("""
    <manifest>
      <projects>
        <project name="Hello-World"
                 remote="https://github.com/Test-Octowin/Hello-World"
                 path="helloworld"/>
        <project name="my_manifest_repo"
                 remote="/tmp/my_manifest_repo"
                 path="my_manifest_repo/test"/>
      </projects>
    </manifest>
    """)

    result = run_list_projects([str(manifest)])
    assert result.exit_code == 0
    assert result.output.splitlines() == [
        "helloworld",
        "my_manifest_repo/test"
    ]


def test_generate_projects(run_create_manifest):
    result = run_create_manifest(["tom6"])
    assert result.exit_code == 0
    # Can we parse the output?
    root = ET.fromstring(result.output)
    assert root.find('./projects') is not None
